package com.rz.im.imtalk.ui.register;

import android.arch.lifecycle.Observer;
import android.databinding.BindingAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.rz.im.core.base.BaseActivity;
import com.rz.im.core.model.api.response.UserInfo;
import com.rz.im.imtalk.R;
import com.rz.im.imtalk.databinding.ActivityRegisterBinding;
import com.rz.im.imtalk.databinding.ActivityRegisterProfileBinding;
import com.rz.im.imtalk.viewmodel.register.RegisterProfileViewModel;
import com.rz.im.imtalk.viewmodel.register.RegisterViewModel;

public class RegisterProfileActivity extends BaseActivity<RegisterProfileViewModel,ActivityRegisterProfileBinding> {
    private ActivityRegisterProfileBinding binding;
    private RegisterProfileViewModel vm;

    @Override
    protected Class<RegisterProfileViewModel> getViewModel() {
        return RegisterProfileViewModel.class;
    }

    @Override
    protected void onCreate(Bundle instance, RegisterProfileViewModel viewModel, ActivityRegisterProfileBinding binding) {
        this.binding = binding;
        this.vm = viewModel;
        this.binding.setListener(this);
        this.binding.setVm(vm);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register_profile;
    }

    public void OnRegisterClick(View view) {

        vm.register().observe(this, new Observer<UserInfo>() {
            @Override
            public void onChanged(@Nullable UserInfo userInfo) {
                Log.i("RayTest","onchange:"+userInfo.toString());
            }
        });
    }

    @BindingAdapter({"updateImg"})
    public static void UpdateImg (View view, String imgurl) {

    }
}
