package com.rz.im.imtalk.ui.register;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.BindingAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.rz.im.core.base.BaseActivity;
import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.cache.CountryInfo;
import com.rz.im.core.model.net.ApiHeaders;
import com.rz.im.imtalk.R;
import com.rz.im.imtalk.databinding.ActivityRegisterBinding;
import com.rz.im.imtalk.ui.TestClass;
import com.rz.im.imtalk.ui.common.CountryListActivity;
import com.rz.im.imtalk.ui.login.LoginActivity;
import com.rz.im.imtalk.viewmodel.register.RegisterUIlogic;
import com.rz.im.imtalk.viewmodel.register.RegisterViewModel;

import java.util.List;

import javax.inject.Inject;

public class RegisterActivity extends BaseActivity<RegisterViewModel,ActivityRegisterBinding> implements  RegisterUIlogic.PrivacyLinkClick {
    private ActivityRegisterBinding binding;
    private RegisterViewModel vm;

    @Inject
    ApiHeaders headers;

    @Inject
    CountryInfo countryInfo;

    @Override
    protected Class getViewModel() {
        return RegisterViewModel.class;
    }

    @Override
    protected void onCreate(Bundle instance, RegisterViewModel viewModel, ActivityRegisterBinding binding) {
        this.binding = binding;
        this.vm = viewModel;
        this.binding.setListener(this);
        this.binding.setVm(vm);
        this.vm.buildprivacy(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }


    public void OnContryClick(View view) {
        if(countryInfo.getCountries()!=null && countryInfo.getCountries().getValue().size()!=0) {
            startCountryListActivity();
        } else {
            vm.country().observe(this, new Observer<List<Country>>() {
                @Override
                public void onChanged(@Nullable List<Country> countries) {
                    countryInfo.setCountries(countries);
                    startCountryListActivity();
                }
            });
        }
    }


    public void OnCheckVerificaionClick(View view) {
        vm.verificaionCheck().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                vm.setRegisterInfo();
                RegisterProfileActivity.start(RegisterActivity.this,null,RegisterProfileActivity.class,null);
                finish();
            }
        });
    }

    public void OnVerificaionCodeClick(View view) {
        Log.i("RayTest","OnVerificaionCodeClick");
        vm.verificaion().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //TODO 之後不支持直接返回驗證碼
                vm.verificationcode.setValue(s);
            }
        });
    }

    @BindingAdapter({"updateZone"})
    public static void updateZone(TextView view , Country item) {
        if(item!=null)
            view.setText(item.getName()+" (+"+item.getZone()+")");
    }

    @BindingAdapter({"SetPrivacylink"})
    public static void SetPrivacylink(TextView view,RegisterActivity activity) {
        view.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClick(RegisterUIlogic.PrivacyEnum privacyEnum) {
        switch (privacyEnum) {
            case PrivacyPolicy:
                break;
            case ServiceAgreement:
                break;
            default:
                break;
        }
    }

    private void startCountryListActivity() {
        CountryListActivity.start(RegisterActivity.this,null,null);
    }

}
