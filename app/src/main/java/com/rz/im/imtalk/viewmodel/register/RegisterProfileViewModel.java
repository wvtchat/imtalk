package com.rz.im.imtalk.viewmodel.register;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.rz.im.core.model.api.request.BodyRegister;
import com.rz.im.core.model.api.response.UserInfo;
import com.rz.im.core.model.cache.RegisterInfo;

import javax.inject.Inject;

public class RegisterProfileViewModel extends ViewModel{

    private final RegisterRepository repository;
    private final RegisterUIlogic uiLogic;
    private final RegisterInfo registerInfo;
    private final BodyRegister bodyRegister;

    public MutableLiveData<String> username = new MutableLiveData<>();
    public MutableLiveData<String> passwd = new MutableLiveData<>();
    public MutableLiveData<String> imgurl = new MutableLiveData<>();

    @Inject
    public RegisterProfileViewModel(RegisterRepository registerRepository,
                                    RegisterUIlogic registerUIlogic, RegisterInfo registerInfo) {

        this.repository = registerRepository;
        this.uiLogic = registerUIlogic;
        this.registerInfo = registerInfo;
        this.bodyRegister = registerInfo.getBodyRegister().getValue();
    }

    public MutableLiveData<UserInfo> register() {
        if(!checkRegisterInfo())
            return new MutableLiveData<>();
        int code = Integer.parseInt(registerInfo.getVerificationCode().getValue());
        Log.i("RayTest","Verifi:"+code);
        Log.i("RayTest","bodyRegister:"+bodyRegister.toString());
        return this.repository.Register(code,bodyRegister);
    }

    public boolean checkRegisterInfo() {
        if(username.getValue().isEmpty() && passwd.getValue().isEmpty())
            return false;
        else {
            Log.i("RayTest","passwd:"+passwd.getValue());
            bodyRegister.setUsername(username.getValue());
            bodyRegister.setPassword(passwd.getValue());
            return true;
        }

    }
}
