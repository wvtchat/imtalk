package com.rz.im.imtalk.viewmodel.login;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.rz.im.core.base.IBaseRepository;
import com.rz.im.core.model.api.method.API;
import com.rz.im.core.model.api.request.BodyLogin;
import com.rz.im.core.model.api.response.Base;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.api.response.UserInfo;
import com.rz.im.core.model.net.ApiHeaders;
import com.rz.im.core.model.schedulers.AppSchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class LoginRepository implements IBaseRepository {

    private final API api;
    private final AppSchedulerProvider schedulerProvider;
    private CompositeDisposable disposables = new CompositeDisposable();
    @Inject
    ApiHeaders headers;

    @Inject
    public LoginRepository(API api , AppSchedulerProvider schedulerProvider) {
        this.api = api;
        this.schedulerProvider = schedulerProvider;
    }


    public MutableLiveData<UserInfo> login(BodyLogin login) {
        MutableLiveData<UserInfo> userinfo = new MutableLiveData<>();
        this.api.Login(login)
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe(new Observer<Base<UserInfo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(Base<UserInfo> userInfoBase) {
                        userinfo.postValue(userInfoBase.getData());
                        Log.i("RayTest","onNext");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("RayTest","onError");
                    }

                    @Override
                    public void onComplete() {
                        Log.i("RayTest","onComplete");
                    }
                });
        return userinfo;
    }

    public MutableLiveData<List<Country>> CountryCode () {
        MutableLiveData<List<Country>> CountrysLiveData = new MutableLiveData<>();
        this.api.CountryCode()
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe(new Observer<Base<List<Country>>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(Base<List<Country>> countries) {
                        CountrysLiveData.postValue(countries.getData());
                        Log.i("RayTest","ok");
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }


                });
        return CountrysLiveData;

    }

    @Override
    public void onClear() {
        disposables.clear();
    }
}
