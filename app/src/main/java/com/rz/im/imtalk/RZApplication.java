package com.rz.im.imtalk;

import android.app.Application;

import com.rz.im.core.base.BaseApplication;

public class RZApplication extends BaseApplication {

    @Override
    public BaseApplication getIntance() {
        return this;
    }
}
