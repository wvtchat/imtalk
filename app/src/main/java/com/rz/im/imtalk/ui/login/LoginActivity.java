package com.rz.im.imtalk.ui.login;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.BindingAdapter;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.rz.im.core.base.BaseActivity;
import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.model.api.request.BodyLogin;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.api.response.UserInfo;
import com.rz.im.core.model.cache.CountryInfo;
import com.rz.im.core.model.net.ApiHeaders;
import com.rz.im.imtalk.BR;
import com.rz.im.imtalk.R;
import com.rz.im.imtalk.databinding.ActivityLoginBinding;
import com.rz.im.imtalk.ui.TestClass;
import com.rz.im.imtalk.ui.common.CountryListActivity;
import com.rz.im.imtalk.ui.register.RegisterActivity;
import com.rz.im.imtalk.viewmodel.login.LoginViewModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;


public class LoginActivity extends BaseActivity<LoginViewModel,ActivityLoginBinding> {


    @Inject
    CountryInfo countryInfo;

    @Inject
    BodyLogin bodyLogin;

    private ActivityLoginBinding binding;
    private LoginViewModel vm;

    @Override
    protected void onCreate(Bundle instance, LoginViewModel viewModel, ActivityLoginBinding binding) {
        this.binding = binding;
        this.vm = viewModel;
        this.binding.setListener(this);
        this.binding.setVm(vm);
    }

    @Override
    protected Class<LoginViewModel> getViewModel() {
        return LoginViewModel.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }


    public void OnContryClick(View view) {
        if(countryInfo.getCountries()!=null && countryInfo.getCountries().getValue().size()!=0) {
            startCountryListActivity();
        } else {
            vm.country().observe(this, new Observer<List<Country>>() {
                @Override
                public void onChanged(@Nullable List<Country> countries) {
                    countryInfo.setCountries(countries);
                    startCountryListActivity();
                }
            });
        }
    }

    private void startCountryListActivity() {
        CountryListActivity.start(LoginActivity.this,null,null);
    }


    public void OnLoginClick(View view) {
        vm.login(bodyLogin).observe(this, new Observer<UserInfo>() {
            @Override
            public void onChanged(@Nullable UserInfo userInfo) {
                Log.i("RayTest","userinfo");
            }
        });
    }

    public void OnRegisterClick(View view) {
        RegisterActivity.start(LoginActivity.this,null, RegisterActivity.class,null);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
           hideSoftKeyboard();
        }
        return super.onTouchEvent(event);
    }

    @BindingAdapter({"updateZone"})
    public static void updateZone(TextView view , Country item) {
        if(item!=null)
           view.setText(item.getName()+" (+"+item.getZone()+")");
    }
}
