package com.rz.im.imtalk.viewmodel.common;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableList;
import android.util.Log;

import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.cache.CountryInfo;

import javax.inject.Inject;
import javax.inject.Named;

public class CountryListViewModel extends ViewModel {


    private CountryInfo countryInfo;
    private ObservableList<BaseBindHolder> data;
    private BaseRecyclerViewAdapter<Country> adapter;

    @Inject
    public CountryListViewModel(BaseRecyclerViewAdapter<Country> adapter, ObservableList<BaseBindHolder> data , CountryInfo countryInfo) {
        this.adapter = adapter;
        this.data = data;
        //this.adapter.initList(data);
        this.countryInfo = countryInfo;

    }

    public BaseRecyclerViewAdapter<Country> getAdapter() {
        return adapter;
    }

    public void setItemResource(int countryResID, int countryVarID) {
        this.adapter.setDataList(countryInfo.getCountries().getValue(),countryResID, countryVarID);
    }

    public void setSelectCountry(Object item) {
        if(item instanceof Country) {
            this.countryInfo.setOnSelectZone((Country)item);
        }
    }
}
