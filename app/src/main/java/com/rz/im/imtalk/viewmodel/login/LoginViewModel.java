package com.rz.im.imtalk.viewmodel.login;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableList;

import com.rz.im.core.base.BaseActivity;
import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.model.api.request.BodyLogin;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.api.response.UserInfo;
import com.rz.im.core.model.cache.CountryInfo;
import com.rz.im.imtalk.ui.common.CountryListActivity;

import java.util.List;

import javax.inject.Inject;

public class LoginViewModel extends ViewModel {

    private BaseRecyclerViewAdapter<Country> adapter;
    private LoginRepository loginRepository;

    public MutableLiveData<Country> Zone  ;
    public MutableLiveData<String> acount = new MutableLiveData<>() ;
    public MutableLiveData<String> password = new MutableLiveData<>();

    @Inject
    public LoginViewModel(LoginRepository loginRepository,   CountryInfo countryInfo) {
        this.loginRepository = loginRepository;
        this.Zone = countryInfo.getOnSelectZone();
    }

    public BaseRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public MutableLiveData<UserInfo> login(BodyLogin bodyLogin) {
        bodyLogin.setCountry(Zone);
        bodyLogin.setMobile(acount.getValue());
        bodyLogin.setPassword(password.getValue());
        return this.loginRepository.login(bodyLogin);
    }
    @Inject
    CountryInfo countryInfo;
    public MutableLiveData<List<Country>> country() {
        return this.loginRepository.CountryCode();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        this.loginRepository.onClear();
    }

}
