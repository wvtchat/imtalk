package com.rz.im.imtalk.viewmodel.register;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.ParcelableSpan;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;

import com.rz.im.imtalk.R;

import javax.inject.Inject;

public class RegisterUIlogic {

    private Context context;
    private PrivacyLinkClick listener;
    public enum PrivacyEnum {
        ServiceAgreement ,PrivacyPolicy
    }

    @Inject
    public RegisterUIlogic(Context context) {
        this.context = context;
    }

    public SpannableStringBuilder BuildPrivacy() {
        Resources res = context.getResources();
        SpannableStringBuilder PrivacyBuilder = new SpannableStringBuilder(BuildPrivacyString());
        ForegroundColorSpan defaultColorSpan = new ForegroundColorSpan(Color.parseColor("#5A94FA"));
        BuildPrivacyPolicy(res,PrivacyBuilder);
        BuildServiceAgreement(res,PrivacyBuilder);
        return PrivacyBuilder;
    }

    private SpannableStringBuilder BuildServiceAgreement(Resources res , SpannableStringBuilder privacyBuilder) {
        String ServiceAgreement = res.getString(R.string.privacy_msg2);
        BuildSpan(new UnderlineSpan(), privacyBuilder, ServiceAgreement);
        BuildSpan(BuildLinkColorSpan() , privacyBuilder ,ServiceAgreement);
        BuildSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                listener.onClick(PrivacyEnum.ServiceAgreement);
            }
        } , privacyBuilder ,ServiceAgreement);
        return  privacyBuilder;
    }

    private SpannableStringBuilder BuildPrivacyPolicy(Resources res , SpannableStringBuilder privacyBuilder) {
        String PrivacyPolicy = res.getString(R.string.privacy_msg3);
        BuildSpan(new UnderlineSpan(), privacyBuilder, PrivacyPolicy);
        BuildSpan(BuildLinkColorSpan() , privacyBuilder ,PrivacyPolicy);
        BuildSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                listener.onClick(PrivacyEnum.PrivacyPolicy);
            }
        } , privacyBuilder ,PrivacyPolicy);
        return  privacyBuilder;
    }


    private ForegroundColorSpan BuildLinkColorSpan() {
        return new ForegroundColorSpan(Color.parseColor("#5A94FA"));
    }

    private SpannableStringBuilder BuildSpan(Object span, SpannableStringBuilder privacyBuilder, String string) {
        String PrivacyString = BuildPrivacyString();
        int start = PrivacyString.indexOf(string);
        int end = PrivacyString.indexOf(string)+string.length();
        privacyBuilder.setSpan(span,start,end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return privacyBuilder;
    }

    private String BuildPrivacyString() {
        Resources res = context.getResources();
        String Privacy1 = res.getString(R.string.privacy_msg1);
        String Privacy2 = res.getString(R.string.privacy_msg2);
        String Privacy3 = res.getString(R.string.privacy_msg3);
        return String.format(Privacy1, Privacy2, Privacy3);
    }

    public void setPrivacyCallback(PrivacyLinkClick _listener) {
        this.listener = _listener;
    }

    public interface PrivacyLinkClick  {

        void onClick(PrivacyEnum privacyEnum);
    }
}
