package com.rz.im.imtalk.viewmodel.register;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.rz.im.core.base.IBaseRepository;
import com.rz.im.core.model.api.method.API;
import com.rz.im.core.model.api.request.BodyRegister;
import com.rz.im.core.model.api.request.BodyVerification;
import com.rz.im.core.model.api.response.Base;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.api.response.RMessage;
import com.rz.im.core.model.api.response.UserInfo;
import com.rz.im.core.model.schedulers.AppSchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RegisterRepository implements IBaseRepository {

    private final API api;
    private final AppSchedulerProvider schedulerProvider;
    private CompositeDisposable disposables = new CompositeDisposable();


    @Inject
    public RegisterRepository(API api , AppSchedulerProvider schedulerProvider ) {
        this.api = api;
        this.schedulerProvider = schedulerProvider;
    }



    @Override
    public void onClear() {
        disposables.clear();
    }

    public MutableLiveData<List<Country>> CountryCode () {
        MutableLiveData<List<Country>> CountrysLiveData = new MutableLiveData<>();
        this.api.CountryCode()
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe(new Observer<Base<List<Country>>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(Base<List<Country>> countries) {
                        CountrysLiveData.postValue(countries.getData());
                    }

                    @Override
                    public void onError(Throwable e) { }

                    @Override
                    public void onComplete() { }
                });
        return CountrysLiveData;

    }

    public MutableLiveData<String> VerificaionCheck(int verificaionCode, BodyVerification body) {
        MutableLiveData<String> VerificaionCheck = new MutableLiveData<>();
        this.api.VerificaionCheck(verificaionCode,body)
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe(new Observer<Base<RMessage>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(Base<RMessage> rMessageBase) {
                        String successMessage = rMessageBase.getData().getMessage();
                        if(successMessage.equals("驗證成功"))
                            VerificaionCheck.setValue(rMessageBase.getData().getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return VerificaionCheck;
    }

    public MutableLiveData<String> VerificaionCode(String zone , String mobile) {
        MutableLiveData<String> VerificaionCode = new MutableLiveData<>();
        Log.i("RayTest","zone:"+zone+" mobile:"+mobile);
        this.api.VerificaionCode(zone,mobile)
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe(new Observer<Base<RMessage>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(Base<RMessage> rMessageBase) {
                        Log.i("RayTest","rMessageBase:"+rMessageBase.getData().getMessage());
                        VerificaionCode.setValue(rMessageBase.getData().getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("RayTest","onError:"+e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.i("RayTest","onComplete:");
                    }
                });
        return VerificaionCode;
    }

    public MutableLiveData<UserInfo> Register (int verificaionCode, BodyRegister body) {
        MutableLiveData<UserInfo> UserInfoData = new MutableLiveData<>();
        this.api.Register(verificaionCode,body)
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe(new Observer<Base<UserInfo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                    }

                    @Override
                    public void onNext(Base<UserInfo> userInfoBase) {
                        Log.i("RayTest","onNext:");
                        UserInfoData.setValue(userInfoBase.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("RayTest","onError:"+e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.i("RayTest","onComplete");
                    }
                });
        return UserInfoData;

    }

}
