package com.rz.im.imtalk.ui.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;

import com.rz.im.core.base.BaseActivity;
import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.imtalk.R;
import com.rz.im.imtalk.databinding.ActivityCountryListBinding;
import com.rz.im.imtalk.databinding.ActivityLoginBinding;
import com.rz.im.imtalk.ui.register.RegisterActivity;
import com.rz.im.imtalk.viewmodel.common.CountryListViewModel;
import com.rz.im.imtalk.viewmodel.login.LoginViewModel;

import javax.inject.Inject;
import javax.inject.Named;

public class  CountryListActivity  extends BaseActivity<CountryListViewModel,ActivityCountryListBinding> implements BaseRecyclerViewAdapter.ClickListener {

    @Inject
    @Named("CountryResID")
    int countryResID ;

    @Inject
    @Named("CountryVarID")
    int countryVarID ;

    private BaseRecyclerViewAdapter<Country> adapter;
    private CountryListViewModel vm;

    @Override
    protected Class<CountryListViewModel> getViewModel() {
        return CountryListViewModel.class;
    }

    @Override
    protected void onCreate(Bundle instance, CountryListViewModel viewModel, ActivityCountryListBinding binding) {
        this.adapter = viewModel.getAdapter();
        this.vm = viewModel;
        binding.setVm(vm);
        binding.setAdapter(adapter);
        this.adapter.setClickListener(this);
        viewModel.setItemResource(countryResID,countryVarID);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_country_list;
    }

    public static void start(Context context, Bundle instance, ActivityOptionsCompat options) {
        Intent intent = new Intent();
        intent.setClass(context,CountryListActivity.class);
        if(instance!=null)
            intent.putExtras(instance);
        context.startActivity(intent);
    }

    @Override
    public void OnAdapterItemClick(BaseBindHolder baseBindHolder, int position) {
        Log.i("RayTeset","pos:"+position);
        vm.setSelectCountry(baseBindHolder.getItem());
        finish();
    }
}
