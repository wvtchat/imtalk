package com.rz.im.imtalk.viewmodel.register;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableList;
import android.text.SpannableStringBuilder;
import android.util.Log;

import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.model.api.request.BodyRegister;
import com.rz.im.core.model.api.request.BodyVerification;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.cache.CountryInfo;
import com.rz.im.core.model.cache.RegisterInfo;
import com.rz.im.imtalk.ui.register.RegisterActivity;

import java.util.List;

import javax.inject.Inject;

public class RegisterViewModel extends ViewModel {


    private CountryInfo countryInfo;
    private RegisterRepository repository;
    private RegisterUIlogic uiLogic;
    //private ObservableList<BaseBindHolder> data;
    private BaseRecyclerViewAdapter<Country> adapter;
    public MutableLiveData<Country> Zone;
    public MutableLiveData<String> mobile = new MutableLiveData<>();
    public MutableLiveData<String> verificationcode;
    public final MutableLiveData<BodyRegister> registerinfo;
    public MutableLiveData<SpannableStringBuilder> privacy = new MutableLiveData<>();


    @Inject
    public RegisterViewModel(RegisterRepository registerRepository,
                             RegisterUIlogic registerUIlogic,
                             CountryInfo countryInfo,
                             RegisterInfo registerInfo) {

        this.repository = registerRepository;
        this.uiLogic = registerUIlogic;
        this.verificationcode = registerInfo.getVerificationCode();
        this.registerinfo = registerInfo.getBodyRegister();
        this.countryInfo = countryInfo;
        this.Zone = countryInfo.getOnSelectZone();
    }

    public BaseRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public MutableLiveData<List<Country>> country() {
        return this.repository.CountryCode();
    }

    public MutableLiveData<String> verificaion() {
        if (Zone.getValue() == null || mobile.getValue() == null) {
            return new MutableLiveData<>();
        }
        return this.repository.VerificaionCode(Zone.getValue().getZone(), mobile.getValue());
    }

    public MutableLiveData<String> verificaionCheck() {
        if (Zone.getValue() == null && mobile.getValue() == null) {
            return new MutableLiveData<>();
        }
        int code = Integer.parseInt(verificationcode.getValue());
        BodyVerification bodyVerification = new BodyVerification(this.Zone.getValue().getZone(), mobile.getValue());
        return this.repository.VerificaionCheck(code, bodyVerification);
    }

    public void buildprivacy(RegisterUIlogic.PrivacyLinkClick listener) {
        this.privacy.setValue(uiLogic.BuildPrivacy());
        uiLogic.setPrivacyCallback(listener);
    }


    public void setRegisterInfo() {
        BodyRegister bodyRegister = registerinfo.getValue();
        bodyRegister.setZone(Zone.getValue().getZone());
        bodyRegister.setMobile(mobile.getValue());
    }
}
