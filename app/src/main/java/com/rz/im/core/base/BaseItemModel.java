package com.rz.im.core.base;

import android.databinding.ViewDataBinding;

import javax.inject.Inject;

public abstract class BaseItemModel{

    public BaseItemModel() {

    }

    public abstract int getLayoutResource() ;

    public abstract int getVariableID() ;

    public abstract void onBinding(ViewDataBinding bind, int position) ;
}
