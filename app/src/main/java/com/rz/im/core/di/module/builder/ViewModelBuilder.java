package com.rz.im.core.di.module.builder;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.rz.im.core.di.common.factory.ViewModelFactory;
import com.rz.im.core.di.common.key.ViewModelKey;
import com.rz.im.imtalk.viewmodel.common.CountryListViewModel;
import com.rz.im.imtalk.viewmodel.login.LoginViewModel;
import com.rz.im.imtalk.viewmodel.register.RegisterProfileViewModel;
import com.rz.im.imtalk.viewmodel.register.RegisterViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelBuilder {

    @Binds
    @IntoMap
    @ViewModelKey(CountryListViewModel.class)
    abstract ViewModel bindsCountryListViewModel(CountryListViewModel countryListViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindsLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel.class)
    abstract ViewModel bindsRegisterViewModel(RegisterViewModel registerViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RegisterProfileViewModel.class)
    abstract ViewModel bindsRegisterProfileViewModel(RegisterProfileViewModel registerProfileViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);

}
