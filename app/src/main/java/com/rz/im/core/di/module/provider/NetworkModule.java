package com.rz.im.core.di.module.provider;

import android.util.Log;

import com.rz.im.core.config.NetConstants;
import com.rz.im.core.model.api.method.API;
import com.rz.im.core.model.net.ApiHeaders;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {


    /**
     * HTTP
     *
     */


    @Provides
    @Singleton
    Interceptor provideInterceptor(ApiHeaders headers) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Log.i("RayTest","token:"+headers.getToken());
                Request original = chain.request();
                Request request = original.newBuilder()
                        .addHeader("token",headers.getToken())
                        .build();
                return chain.proceed(request);
            }
        };
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttp(Interceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(NetConstants.APIURL)
                .client(client)
                .build();
    }

    /**
     * API
     *
     */

    @Singleton
    @Provides
    public API provideLoginAPI(Retrofit retrofit){
        return retrofit.create(API.class);
    }

}