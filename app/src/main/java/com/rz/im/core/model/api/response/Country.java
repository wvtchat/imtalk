package com.rz.im.core.model.api.response;

public class Country{

    /**
     * zone : 00886
     * name : 台灣
     */
    public String zone;
    public String name;

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Country{" +
                "zone='" + zone + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
