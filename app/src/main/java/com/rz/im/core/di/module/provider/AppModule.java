package com.rz.im.core.di.module.provider;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.view.inputmethod.InputMethodManager;

import com.rz.im.core.base.BaseApplication;
import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.base.listener.CallbackObservableList;
import com.rz.im.core.config.DataConstants;
import com.rz.im.core.di.common.scopes.ActivityScope;
import com.rz.im.core.di.module.builder.ViewModelBuilder;
import com.rz.im.core.di.module.provider.NetworkModule;
import com.rz.im.core.model.net.ApiHeaders;
import com.rz.im.core.model.schedulers.AppSchedulerProvider;
import com.rz.im.imtalk.ui.TestClass;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.INPUT_METHOD_SERVICE;

@Module(includes = {NetworkModule.class, ViewModelBuilder.class, CommonModule.class, UserModule.class})
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(BaseApplication application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    SharedPreferences provideSplashViewModel(Context context) {
        return context.getSharedPreferences(DataConstants.PREFERENCES, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    AppSchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }


    @Provides
    @Singleton
    ApiHeaders provideHeader() {
        return new ApiHeaders();
    }

    @Provides
    @Singleton
    InputMethodManager provideHeaderImm(Context context) {
        return  (InputMethodManager)context.getSystemService(INPUT_METHOD_SERVICE);
    }
}
