package com.rz.im.core.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.rz.im.core.base.listener.CallbackObservableList;
import com.rz.im.core.di.common.factory.BindHolderFactory;
import com.rz.im.core.model.api.response.Country;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;


public class BaseRecyclerViewAdapter<item> extends RecyclerView.Adapter<BaseRecyclerViewAdapter.ViewHolder> {

    private BaseRecyclerViewAdapter.ClickListener clickListener;
    private CallbackObservableList observableList;
    private ObservableList<BaseBindHolder> data;

    @Inject
    BindHolderFactory factory;

    @Inject
    public BaseRecyclerViewAdapter(CallbackObservableList observableList ,ObservableList<BaseBindHolder> _data) {
        this.observableList = observableList;
        this.data = _data;
        observableList.setAdapter(this);
        initList();
    }

    public void initList() {
        //this.data = data;
        Log.i("RayTest","init");
        data.addOnListChangedCallback(this.observableList);
    }

    public void setDataList(List<item> countries, int countryResID, int countryVarID) {
        List<BaseBindHolder> holders = factory.createBaseBindHolders(countries, countryResID, countryVarID );
        this.data.addAll(holders);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        LayoutInflater mLayoutInflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(DataBindingUtil.inflate(mLayoutInflater,data.get(position).getLayoutResource(),parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ViewDataBinding dataBinding = holder.getBinding();
        Object item = data.get(position).getItem();
        if(item instanceof BaseItemModel) {
            ((BaseItemModel)item).onBinding(dataBinding,position);
        }
        dataBinding.setVariable(data.get(position).getVariableID(), data.get(position).getItem());
        dataBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class ViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder implements View.OnClickListener {


        private final T binding;

        ViewHolder(T dataBinding) {
            super(dataBinding.getRoot());
            this.binding = dataBinding;
            this.binding.getRoot().setOnClickListener(this);
        }

        public T getBinding () {
            return this.binding;
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(clickListener!=null)
                clickListener.OnAdapterItemClick(data.get(position),position);
        }
    }

    public interface ClickListener<T> {

        void OnAdapterItemClick(BaseBindHolder baseBindHolder, int position);
    }

}
