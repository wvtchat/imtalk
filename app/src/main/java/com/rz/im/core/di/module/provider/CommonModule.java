package com.rz.im.core.di.module.provider;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.v7.widget.LinearLayoutManager;

import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.base.listener.CallbackObservableList;
import com.rz.im.core.di.common.factory.BindHolderFactory;
import com.rz.im.core.di.common.scopes.ActivityScope;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.cache.CountryInfo;
import com.rz.im.core.model.cache.RegisterInfo;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CommonModule {

    @Provides
    public ObservableList<BaseBindHolder> provideBaseBindHolders() {
        return new ObservableArrayList<>();
    }

    @Provides
    public LinearLayoutManager provideLayoutParam(Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    public CallbackObservableList provideCallbackObservableList() {
        return new CallbackObservableList();
    }

    @Provides
    @Singleton
    public CountryInfo provideCountryList() {
        return new CountryInfo();
    }

    @Provides
    @Singleton
    public RegisterInfo provideRegisterInfo() {
        return new RegisterInfo();
    }
}
