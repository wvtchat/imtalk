package com.rz.im.core.model.net;

import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;

public class ApiHeaders {

    String token = "";

    public ApiHeaders() {
        Log.i("RayTest","apiheader build"+this.toString());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
