package com.rz.im.core.model.api.request;

public class BodyVerification {

    private String zone;
    private String mobile;

    public BodyVerification(String zone, String mobile) {
        this.zone = zone;
        this.mobile = mobile;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
