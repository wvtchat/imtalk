package com.rz.im.core.di.module.builder;

import com.rz.im.core.di.common.scopes.ActivityScope;
import com.rz.im.core.di.module.provider.CountryListModule;
import com.rz.im.core.di.module.provider.LoginActivityModule;
import com.rz.im.core.di.module.provider.UserModule;
import com.rz.im.imtalk.ui.common.CountryListActivity;
import com.rz.im.imtalk.ui.login.LoginActivity;
import com.rz.im.imtalk.ui.register.RegisterActivity;
import com.rz.im.imtalk.ui.register.RegisterProfileActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    //登入頁面
    @ActivityScope
    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity contributeLoginActivity();

    //登入頁面
    @ActivityScope
    @ContributesAndroidInjector
    abstract RegisterActivity contributeRegisterActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract RegisterProfileActivity contributeRegisterProfileActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = CountryListModule.class)
    abstract CountryListActivity contributeCountryListActivity();





}
