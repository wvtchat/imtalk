package com.rz.im.core.base;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

public class BaseBindHolder<T> {

    private T item;
    private int layoutResource;
    private int variableID;
    @Inject
    public BaseBindHolder(T _item, @Named("resID") int layoutResource, @Named("varID") int variableID) {
        this.item = _item;
        this.layoutResource = layoutResource;
        this.variableID = variableID;
    }

    public BaseBindHolder(T item) {
        if(item instanceof BaseItemModel) {
            this.item = item;
            this.layoutResource = ((BaseItemModel) item).getLayoutResource();
            this.variableID = ((BaseItemModel) item).getVariableID();
        }
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public void setlayoutResource(int layoutResource) {
        this.layoutResource = layoutResource;
    }

    public int getLayoutResource() {
        return layoutResource;
    }

    public void setLayoutResource(int layoutResource) {
        this.layoutResource = layoutResource;
    }

    public void setVariableID(int variableID) {
        this.variableID = variableID;
    }

    public int getVariableID() {
        return variableID;
    }
}
