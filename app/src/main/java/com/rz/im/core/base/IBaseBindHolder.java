package com.rz.im.core.base;

import android.databinding.ObservableList;

import java.util.List;

public interface IBaseBindHolder {

    List<BaseBindHolder> createBaseBindHolders(List<?> countries,int resID, int varID);
}
