package com.rz.im.core.di.component;


import com.rz.im.core.base.BaseApplication;
import com.rz.im.core.di.module.builder.ActivityBuilder;
import com.rz.im.core.di.module.provider.AppModule;
import com.rz.im.core.di.module.provider.UserModule;

import javax.inject.Singleton;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class})

public interface AppComponent extends AndroidInjector<BaseApplication>{

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseApplication>{};

}
