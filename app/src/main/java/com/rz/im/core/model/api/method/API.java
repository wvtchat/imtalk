package com.rz.im.core.model.api.method;

import com.rz.im.core.model.api.request.BodyLogin;
import com.rz.im.core.model.api.request.BodyRegister;
import com.rz.im.core.model.api.request.BodyVerification;
import com.rz.im.core.model.api.response.Base;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.api.response.RMessage;
import com.rz.im.core.model.api.response.UserInfo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface API {


    //城市碼
    @GET("utils/countrys")
    Observable<Base<List<Country>>> CountryCode();

    //註冊
    @POST("users/register/code/{Verification_code}")
    Observable<Base<UserInfo>> Register(@Path("Verification_code") int verificationCode,
                              @Body BodyRegister bodyRegister);

    //获取注册验证码
    @GET("sms")
    Observable<Base<RMessage>> VerificaionCode(@Query("zone") String noncestr,
                                               @Query("mobile") String phones);

    //確認驗證碼
    @POST("sms/verification/code/{Verification_code}")
    Observable<Base<RMessage>> VerificaionCheck(@Path("Verification_code") int verificationCode, @Body BodyVerification verificationcode);

    //登入
    @POST("users/login/sessions")
    Observable<Base<UserInfo>> Login(@Body BodyLogin bodyLogin);

}

