package com.rz.im.core.base.listener;

import android.databinding.ObservableList;
import android.util.Log;

import com.rz.im.core.base.BaseRecyclerViewAdapter;

import javax.inject.Inject;


public class CallbackObservableList<T> extends ObservableList.OnListChangedCallback<ObservableList<T>>{

    private BaseRecyclerViewAdapter adapter;

    @Inject
    public CallbackObservableList() {
    }

    public void setAdapter (BaseRecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void onChanged(ObservableList<T> sender) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
        adapter.notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
        adapter.notifyItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
        adapter.notifyItemRangeRemoved(positionStart, itemCount);
    }
}