package com.rz.im.core.di.module.provider;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.util.Log;

import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseItemModel;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.base.listener.CallbackObservableList;
import com.rz.im.core.di.common.factory.BindHolderFactory;
import com.rz.im.core.di.common.scopes.ActivityScope;
import com.rz.im.core.model.api.request.BodyLogin;
import com.rz.im.core.model.api.response.Country;
import com.rz.im.core.model.net.ApiHeaders;
import com.rz.im.imtalk.BR;
import com.rz.im.imtalk.R;
import com.rz.im.imtalk.ui.login.LoginActivity;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginActivityModule {

    @Provides
    @ActivityScope
    public BodyLogin provideBodyLogin() {
        return new BodyLogin();
    }
}
