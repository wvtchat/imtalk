package com.rz.im.core.model.api.request;

import android.arch.lifecycle.MutableLiveData;

import com.rz.im.core.model.api.response.Country;

public class BodyLogin {
    String zone;
    String mobile;
    String password;

    public BodyLogin() {
    }

    public BodyLogin(String zone, String mobile, String password) {
        this.zone = zone;
        this.mobile = mobile;
        this.password = password;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCountry(MutableLiveData<Country> zone) {
        if(zone.getValue()==null)
            this.zone = "";
        else
            this.zone = zone.getValue().getZone();
    }
}
