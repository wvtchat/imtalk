package com.rz.im.core.di.module.provider;
import com.rz.im.imtalk.ui.TestClass;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @Provides
    @Singleton
    TestClass provideTestClass() {
        return new TestClass();
    }
}
