package com.rz.im.core.model.api.response;

public class UserInfo {


    /**
     * Token : 60864896-f8f6-4b47-9901-4b6367a34426
     * IMToken : 168d8426-caf7-478b-8b3c-bff040cf5b1b
     * User : {"open_id":"3483e3184677ded5a923b6e3fe01963e","username":"Hugo","user_id":"","zone":"00886","mobile":"0911810212","avatar":"","sex":0,"password":"d7ae8d905bb7f4ce6a7e30e0b64131a0","email":"","area":"","status":0}
     */

    private String Token;
    private String IMToken;
    private UserBean User;

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    public String getIMToken() {
        return IMToken;
    }

    public void setIMToken(String IMToken) {
        this.IMToken = IMToken;
    }

    public UserBean getUser() {
        return User;
    }

    public void setUser(UserBean User) {
        this.User = User;
    }

    public static class UserBean {
        /**
         * open_id : 3483e3184677ded5a923b6e3fe01963e
         * username : Hugo
         * user_id :
         * zone : 00886
         * mobile : 0911810212
         * avatar :
         * sex : 0
         * password : d7ae8d905bb7f4ce6a7e30e0b64131a0
         * email :
         * area :
         * status : 0
         */

        private String open_id;
        private String username;
        private String user_id;
        private String zone;
        private String mobile;
        private String avatar;
        private int sex;
        private String password;
        private String email;
        private String area;
        private int status;

        public String getOpen_id() {
            return open_id;
        }

        public void setOpen_id(String open_id) {
            this.open_id = open_id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        @Override
        public String toString() {
            return "UserBean{" +
                    "open_id='" + open_id + '\'' +
                    ", username='" + username + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", zone='" + zone + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", sex=" + sex +
                    ", password='" + password + '\'' +
                    ", email='" + email + '\'' +
                    ", area='" + area + '\'' +
                    ", status=" + status +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "Token='" + Token + '\'' +
                ", IMToken='" + IMToken + '\'' +
                ", User=" + User +
                '}';
    }
}
