package com.rz.im.core.model.api.response;

public class Base<DataType> {
    int Status;
    DataType Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public DataType getData() {
        return Data;
    }

    public void setData(DataType data) {
        Data = data;
    }
}
