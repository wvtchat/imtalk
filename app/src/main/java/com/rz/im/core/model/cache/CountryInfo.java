package com.rz.im.core.model.cache;

import android.arch.lifecycle.MutableLiveData;

import com.rz.im.core.model.api.response.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryInfo {

    private MutableLiveData<List<Country>> countries = new MutableLiveData<>();
    private MutableLiveData<Country> onSelectZone = new MutableLiveData<>();

    public MutableLiveData<List<Country>> getCountries() {
        if(countries.getValue()==null)
            countries.setValue(new ArrayList<>());
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries.setValue(countries);
    }

    public void setOnSelectZone(Country item) {
        this.onSelectZone.setValue(item);
    }

    public MutableLiveData<Country> getOnSelectZone() {
        return onSelectZone;
    }
}
