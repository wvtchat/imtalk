package com.rz.im.core.model.cache;

import android.arch.lifecycle.MutableLiveData;

import com.rz.im.core.model.api.request.BodyRegister;
import com.rz.im.core.model.api.response.Country;

public class RegisterInfo {

    private MutableLiveData<String> verificationCode = new MutableLiveData<>();;
    private MutableLiveData<BodyRegister> bodyRegister = new MutableLiveData<>();

    public MutableLiveData<String> getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode.setValue(verificationCode);
    }

    public MutableLiveData<BodyRegister> getBodyRegister() {
        if(bodyRegister.getValue()==null)
            bodyRegister.setValue(new BodyRegister());
        return bodyRegister;
    }

    public void setBodyRegister(BodyRegister bodyRegister) {
        this.bodyRegister.setValue(bodyRegister);
    }
}
