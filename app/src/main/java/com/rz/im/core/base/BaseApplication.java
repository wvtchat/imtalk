package com.rz.im.core.base;

import android.app.Application;


import com.rz.im.core.di.component.DaggerAppComponent;


import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public abstract class BaseApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(getIntance());

    }

    public abstract BaseApplication getIntance() ;
}
