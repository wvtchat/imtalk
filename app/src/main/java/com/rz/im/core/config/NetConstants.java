package com.rz.im.core.config;

public class NetConstants {
    public static final String BASE_URL = "http://14.18.253.121:8080/";
    public static final String VERSION_V1 = "v1/";
    public static final String APIURL = BASE_URL + VERSION_V1;
}
