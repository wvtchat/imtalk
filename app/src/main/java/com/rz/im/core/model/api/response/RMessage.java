package com.rz.im.core.model.api.response;

public class RMessage {

    /**
     * Message : 發送成功
     */

    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}
