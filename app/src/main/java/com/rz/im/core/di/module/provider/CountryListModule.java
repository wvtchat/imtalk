package com.rz.im.core.di.module.provider;

import android.databinding.ObservableList;

import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.BaseRecyclerViewAdapter;
import com.rz.im.core.base.listener.CallbackObservableList;
import com.rz.im.core.di.common.scopes.ActivityScope;
import com.rz.im.imtalk.BR;
import com.rz.im.imtalk.R;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class CountryListModule {

    @Provides
    @ActivityScope
    public BaseRecyclerViewAdapter provideBaseRecyclerViewAdapter(CallbackObservableList observableList , ObservableList<BaseBindHolder> data) {
        return new BaseRecyclerViewAdapter(observableList,data);
    }

    @Provides
    @Named("CountryResID")
    public int provideCountryResID () {
        return R.layout.item_login_country;
    }

    @Provides
    @Named("CountryVarID")
    public int provideCountryVarID () {
        return BR.item_country;
    }
}
