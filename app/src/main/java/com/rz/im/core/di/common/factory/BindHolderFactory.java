package com.rz.im.core.di.common.factory;

import android.databinding.ObservableList;

import com.rz.im.core.base.BaseBindHolder;
import com.rz.im.core.base.IBaseBindHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BindHolderFactory implements IBaseBindHolder {


    @Inject
    public BindHolderFactory() {
    }

    @Override
    public List<BaseBindHolder> createBaseBindHolders(List<?> datas,int resID, int varID) {
        List<BaseBindHolder> datalist = new ArrayList<>();
        for (Object data : datas) {
            BaseBindHolder bindHolder = new BaseBindHolder(data,resID,varID);
            datalist.add(bindHolder);
        }
        return datalist;
    }
}
