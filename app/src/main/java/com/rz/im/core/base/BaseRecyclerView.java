package com.rz.im.core.base;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import javax.inject.Inject;

public class BaseRecyclerView extends RecyclerView {

    private Context context;

    @Inject
    LinearLayoutManager layoutManager;

    public BaseRecyclerView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public BaseRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setLayoutManager(layoutManager);
    }
}
